
tgNamespace.tgContainer.registerInnerGate("advancedConversion-tracking", function(oWrapper, oLocalConfData)
{
    this.oWrapper = oWrapper;
    this.oLocalConfData = oLocalConfData;


    this._construct = function()
    {
        if(this.oWrapper.debug("advancedConversion-tracking","_construct"))debugger;

        this.oWrapper.registerInnerGateFunctions("advancedConversion-tracking", "sale", this.trackSale);
        this.oWrapper.registerInnerGateFunctions("advancedConversion-tracking", "pageview", this.trackPageView);

    }

    this.trackPageView = function(oArgs) {
        if(this.oWrapper.debug("advancedConversion-tracking","trackPageView"))debugger;


        var urlparam;

        if(typeof this.oLocalConfData.affiliate !== "undefined"){
            urlparam = this.oWrapper.fGetUrlParams();
            if(typeof urlparam[this.oLocalConfData.affiliate] !== "undefined"){
            this.oWrapper.setCookie(this.oLocalConfData.affiliate, urlparam[this.oLocalConfData.affiliate], this.oLocalConfData.cookieDuration);
            }
        }

    }




    this.trackSale = function(oArgs)
    {
        if(this.oWrapper.debug("advancedConversion-tracking","trackSale"))debugger;

        var cookieValue = this.oWrapper.readCookie(this.oLocalConfData.affiliate);


        // Create iFrame
        this.iFrame = document.createElement("iframe");
        this.iFrame.width = 1;
        this.iFrame.height = 1;
        this.iFrame.scrolling = "no";
        this.iFrame.frameborder = 0;
        this.iFrame.style = "display:none";




        var iframe_url = "//www.kdukvh.com/tags/c?containerTagId=" + oLocalConfData.containertag_id + "&cid=" + oLocalConfData.cid +
            "&type=" + oLocalConfData.type + "&oid="+ oArgs.orderid;

        for (i = 0; i < oArgs.products.length; i++) {
            var num = parseInt(oArgs.products[i].brutto)/100 *(100-oLocalConfData.ust);
            var amount_netto = num.toFixed(2);
            iframe_url+= "&item"+[i+1]+"="+oArgs.products[i].variationid+"&qty"+[i+1]+"="+oArgs.products[i].quantity+"&amt"+[i+1]+"="+ amount_netto;
        }


        if (cookieValue !== null) {
            iframe_url += "&currency=CZK" + "&name=Hervis.cz_itemsale" + "&CJEVENT=" + cookieValue;

            this.iFrame.src = encodeURI(iframe_url);

            // Append and Fire iFrame
            var oFirstScript = document.getElementsByTagName("script")[0];
            oFirstScript.parentNode.insertBefore(this.iFrame, oFirstScript);


            // Reset Products & Cookie
            this.oWrapper.setCookie(this.oLocalConfData.affiliate, 0, -1);
            oArgs.products = [];


        }
        else{
            iframe_url += "&currency=CZK" + "&name=Hervis.cz_itemsale";
            this.iFrame.src = encodeURI(iframe_url);

            // Append and Fire iFrame
            var oFirstScript = document.getElementsByTagName("script")[0];
            oFirstScript.parentNode.insertBefore(this.iFrame, oFirstScript);

        }


    }




});



